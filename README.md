# What does the python script do?
1. The script uses CANVAS LMS **REST API** to authenticate into the server using a specific API token and obtains the download link for each file. 
2. There are three specific scenarios to go over before the program downloads the file and places it under the correct subject folder:
  a. f the file doesn't exist --> The file is downloaded 
  b. If the file exists --> Is it an older version compared to the file on CANVAS server
      1. If YES --> the new file is downloaded
      2. If NO --> The program compares the file size on server and local drive and checks if the download was interrupted. If it was, the file is re-downloaded

**Note**
1. Refer to https://canvas.instructure.com/doc/api/file.oauth.html#manual-token-generation to get information on generating an API access token.
2. Change the **link-to-canvas-site** in **config.json** to your appropriate link to canvas lms site.
3. Add the API access token in **KEY** location in **api.json**.

# TODO (Depending on available time)
1. Restructure code according to proper python rules and syntax format (PEP 8).
2. Authenticate using OAUTH method instead of an API token (OAUTH is mandatory if the script / program is goind to be released to end-users).
3. Create a method to check for new changes if you have already used the script once (similar to a cloud syncing program).
4. Create a GUI for easier use of the program.

# Feedback
1. If you have any feedback regarding bugs, feature requests, kindly add to the issue list.