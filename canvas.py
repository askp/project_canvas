'''
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

'''
Author: Abhinav Suresh Kumar
Contributions:
contrib_1. Requests session: https://stackoverflow.com/questions/32986228/difference-between-using-requests-get-and-requests-session-get
contrib_2. Create Folder: https://stackoverflow.com/questions/273192/how-can-i-create-a-directory-if-it-does-not-exist/14364249#14364249
https://community.canvaslms.com/thread/19263-pagination-makes-endless-api-calls-when-retrieving-pageviews
https://canvas.instructure.com/doc/api/file.pagination.html
http://docs.python-requests.org/en/master/user/advanced/

'''

# Importing required libraries
import os
import pathlib
import datetime
import time
import re
import json
import requests
from bs4 import BeautifulSoup
from clint.textui import progress

class CanvasSydney:
    
    def __init__(self):
        self.serverDateModified = 'Last-Modified'
        self.fileOpenFormat = 'wb'
        self.LinkHeader = 'Link'
        self.htmlURLNext = 'next'
        self.htmlURLLast = 'last'

    def loadJSONData(self, file):
        with open(file, 'r') as f:
            data = json.load(f)
            return data
    
    def getJSONData(self):
        configJSON = self.loadJSONData('config.json')
        apiJSON = self.loadJSONData('api.json')
        url = configJSON['canvas']['url']
        module = configJSON['canvas']['module']
        Items = configJSON['canvas']['item']
        itemNumber = configJSON['canvas']['itemNumber']
        key = apiJSON['api']['key']
        return url, key, module, Items, itemNumber

    def getCanvasData(self, url, key, headers=False):      
        r = requests.get(url, headers = {'Authorization': 'Bearer ' + '%s' % key})
        value = r.json()
        return value 

    def createFolder(self, folderpath):
        #contrib_2
        pathlib.Path(folderpath).mkdir(parents=True, exist_ok=True)

    def saveDownloadData(self, filepath, requestsContent, fileSize, chunkSize):
        # contrib: find link for this sample code
        with open(filepath,self.fileOpenFormat) as g:
            for chunk in progress.bar(requestsContent.iter_content(chunk_size=chunkSize), expected_size=(fileSize/chunkSize)+1):
                if chunk:
                    g.write(chunk)
                    g.flush()
    
    def getFileGMTtime(self, filepath):    
        return os.path.getmtime(filepath) 

    def getServerFileGMTTime(self, requestsContent):
        serverFileTime = time.mktime(datetime.datetime.strptime(requestsContent.headers[self.serverDateModified], "%a, %d %b %Y %X GMT").timetuple())
        return serverFileTime

    def changeDirectory(self, location):
        os.chdir(location)

    def getCurrentDirectory(self):
        return os.getcwd()

    def fileDiff(self, requestObject):
        htmlURLContent = 'Content-Disposition'
        htmlURLSize = 'Content-Length'
        fileNameRegex = r'"(.*?)"'
        chunkSize = 1024
        for headers in requestObject.headers:
            if headers == htmlURLContent:
                fileHeaders = requestObject.headers[htmlURLContent]
                fileSize = int(requestObject.headers[htmlURLSize])
                fileName = str(''.join(re.findall(fileNameRegex, fileHeaders)))
                print(fileName)
                serverFileTime = self.getServerFileGMTTime(requestObject)
                if os.path.isfile(fileName):
                    localFileSize = os.stat(fileName)
                    if self.getFileGMTtime(fileName) < serverFileTime:
                        self.saveDownloadData(fileName, requestObject, fileSize, chunkSize)
                    else:
                        if localFileSize.st_size == fileSize:
                            pass
                        else:
                            self.saveDownloadData(fileName, requestObject, fileSize, chunkSize)
                else:
                    self.saveDownloadData(fileName, requestObject, fileSize, chunkSize)

    '''
    def getNextPage(self, url, key, requestRawData):
        data = []

        for i in requestRawData:
            data.append(i)

        # https://community.canvaslms.com/thread/19263-pagination-makes-endless-api-calls-when-retrieving-pageviews
        try:
            rawData = requests.head(URL, headers =  {'Authorization': 'Bearer ' + '%s' % KEY})
            for headers in rawData.headers:
                if headers == self.LinkHeader:
                    while self.htmlURLNext in rawData.links:
                        newRawData = self.getCanvasData(self.URL, self.KEY, rawData.links[self.htmlURLNext]['url'], False)
                        for k in newRawData:
                            data.append(k)

                        if rawData.links[self.htmlURLNext]['url'] == rawData.links[self.htmlURLLast]['url']:
                            break    
        except: 
            pass
        return data
    '''

    def prettyJSON(self, jsondata):
        # contrib: get source for this sample code
        return json.dumps(jsondata, indent = 4, sort_keys=True)
        
    def getFile(self):
        #contrib_1:
        url, key, module, item, itemNumber = self.getJSONData()
        folderName = 'name'
        folderTitle = 'title'
        linkId = 'id'
        dataUrl = 'url'
        fileDownloadGroup = 'body'
        fileParser = 'html.parser'
        htmlURLBody = 'a'
        htmlURL = 'href'
        pattern = 'download'

        initialDirectory = os.getcwd()
        firstLevel = ''
        secondLevel = ''
        canvas = self.getCanvasData(url, key)
        for i in canvas: 
            self.changeDirectory(initialDirectory)
            print(os.getcwd())
            self.createFolder(i[folderName])
            courseData = self.getCanvasData(module % i[linkId], key)
            # insert self.getNextPage for each time you go a getCanvasData to be safe
            for j in courseData:
                os.chdir(initialDirectory)
                os.chdir(i[folderName]) 
                print(os.getcwd())
                firstLevel = os.getcwd()             
                self.createFolder(j[folderName])
                subjectData = self.getCanvasData(item %(i[linkId], j[linkId]), key)
                for k in subjectData:
                    os.chdir(firstLevel)
                    os.chdir(j[folderName])
                    secondLevel = os.getcwd()         
                    # Desktop / ICDA / folders
                    self.createFolder(k[folderTitle])
                    os.chdir(k[folderTitle])
                    print(os.getcwd()) #################
                    folderData = self.getCanvasData(itemNumber %(i[linkId], j[linkId], k[linkId]), key)
                    # No need for a for loop as you only get a single line of JSON data (i.e. the URL to the correct page) from above mentioned line
                    notesData = self.getCanvasData(folderData[dataUrl], key)
                    try:
                        downloadLinkPack = BeautifulSoup(notesData[fileDownloadGroup], fileParser)
                    except:
                        pass

                    if len(downloadLinkPack) != 0:
                        for link in downloadLinkPack.find_all(htmlURLBody):
                            if re.search(pattern, link.get(htmlURL)):                            
                                try:
                                    r = requests.Session()
                                    response2 = r.get(link.get(htmlURL))
                                    self.fileDiff(response2)
                                except requests.exceptions.RequestException as identifier:
                                    print(identifier)                     

data = CanvasSydney()
data.getFile()
